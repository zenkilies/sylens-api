FROM golang:latest

LABEL maintainer="Nguyen Dinh <zenkilies@gmail.com>"

WORKDIR /app

COPY go.mod go.sum ./

RUN go mod download

COPY . .

RUN go build -o main .

EXPOSE 1323

CMD ["./main"]
