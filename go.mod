module gitlab.com/zenkilies/sylens-api

go 1.12

require (
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo/v4 v4.1.16
	github.com/stretchr/testify v1.4.0
	go.mongodb.org/mongo-driver v1.3.3
	golang.org/x/crypto v0.0.0-20200221231518-2aa609cf4a9d
)
