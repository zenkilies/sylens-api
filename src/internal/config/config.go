package config

import (
	"os"
	"strconv"
	"strings"
)

func GetString(k Key, defaultVal string) string {
	if v := os.Getenv(string(k)); v != "" {
		return v
	}

	return defaultVal
}

func GetInt(k Key, defaultVal int) int {
	if v := os.Getenv(string(k)); v != "" {
		if i, err := strconv.Atoi(v); err == nil {
			return i
		}

		return defaultVal
	}

	return defaultVal
}

func GetBool(k Key, defaultVal bool) bool {
	if v := os.Getenv(string(k)); v != "" {
		if v == "1" || strings.ToLower(v) == "true" {
			return true
		}

		return false
	}

	return defaultVal
}
