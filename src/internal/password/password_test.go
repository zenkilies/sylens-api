package password

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGenerateHashFromPassword(t *testing.T) {
	password := "PASSWORD"
	hash := GenerateHashFromPassword("PASSWORD")

	assert.NotEqual(t, password, hash)
	assert.NotEqual(t, "", hash)
}

func TestCompareHashAndPassword(t *testing.T) {
	password := "PASSWORD"
	hash := GenerateHashFromPassword(password)

	assert.True(t, CompareHashAndPassword(hash, password))
}
