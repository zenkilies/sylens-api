package storage

type Paste struct {
	Slug      string `bson:"slug"`
	Content   string `bson:"content"`
	Password  string `bson:"password"`
	CreatedAt int64  `bson:"created_at"`
	UpdatedAt int64  `bson:"updated_at"`
}
