package storage

import (
	"context"
	"errors"

	"go.mongodb.org/mongo-driver/bson"

	"gitlab.com/zenkilies/sylens-api/src/external/mongodb"
)

const (
	PASTE_COLLECTION_NAME = "pastes"
)

//go:generate mockery -name=PasteStorage -inpkg -case=snake
type PasteStorage interface {
	Insert(context.Context, Paste) error
	FindBySlug(context.Context, string) (*Paste, error)
}

func NewPasteStorage(mongoClient mongodb.Client) (PasteStorage, error) {
	if mongoClient == nil {
		return nil, errors.New("missing dependencies: `mongodb.Client`")
	}

	return &pasteStorageImpl{
		mongoClient: mongoClient,
	}, nil
}

type pasteStorageImpl struct {
	mongoClient mongodb.Client
}

func (i *pasteStorageImpl) Insert(ctx context.Context, p Paste) error {
	c := i.mongoClient.Collection(PASTE_COLLECTION_NAME)

	_, err := c.InsertOne(ctx, p)
	if err != nil {
		return err
	}

	return nil
}

func (i *pasteStorageImpl) FindBySlug(ctx context.Context, slug string) (*Paste, error) {
	c := i.mongoClient.Collection(PASTE_COLLECTION_NAME)
	p := &Paste{}

	filterBson := bson.M{
		"slug": slug,
	}

	err := c.FindOne(ctx, filterBson).Decode(&p)
	if err != nil {
		return nil, err
	}

	return p, nil
}
