package mongodb

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/zenkilies/sylens-api/src/internal/config"
)

//go:generate mockery -name=Client -inpkg -case=snake
type Client interface {
	Collection(string, ...*options.CollectionOptions) *mongo.Collection
}

func NewClient() (Client, error) {
	opts := options.Client().ApplyURI(config.GetString(config.DATABASE_DSN, "mongodb://127.0.0.1:27017"))

	client, err := mongo.Connect(context.Background(), opts)
	if err != nil {
		return nil, err
	}

	db := client.Database(config.GetString(config.DATABASE_NAME, "sylens-dev"))

	return &clientImpl{
		client: client,
		db:     db,
	}, nil
}

type clientImpl struct {
	client *mongo.Client
	db     *mongo.Database
}

func (c *clientImpl) Collection(name string, opts ...*options.CollectionOptions) *mongo.Collection {
	return c.db.Collection(name, opts...)
}
