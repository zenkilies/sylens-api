package recaptcha

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/zenkilies/sylens-api/src/internal/config"
)

//go:generate mockery -name=Client -inpkg -case=snake
type Client interface {
	Verify(string) (bool, error)
}

func NewClient() (Client, error) {
	return &clientImpl{}, nil
}

type clientImpl struct {
}

func (i *clientImpl) Verify(token string) (bool, error) {
	if config.GetBool(config.CAPTCHA_BYPASS, false) {
		return true, nil
	}

	resp, err := i.verifyReCAPTCHA(verifyReCAPTCHAParams{
		Secret: config.GetString(config.CAPTCHA_SECRET, ""),
		Token:  token,
	})
	if err != nil {
		return false, err
	}

	if resp.Success != true || resp.Score < 0.5 || resp.Hostname != config.GetString(config.CAPTCHA_HOSTNAME, "") {
		return false, nil
	}

	return true, nil
}

func (i *clientImpl) verifyReCAPTCHA(req verifyReCAPTCHAParams) (*verifyReCAPTCHAResult, error) {
	b := []byte(fmt.Sprintf("secret=%s&response=%s", req.Secret, req.Token))

	resp, err := http.Post("https://www.google.com/recaptcha/api/siteverify", "application/x-www-form-urlencoded", bytes.NewBuffer(b))
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != 200 {
		return nil, fmt.Errorf("un-expected status code: %d", resp.StatusCode)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var v verifyReCAPTCHAResult

	err = json.Unmarshal(body, &v)
	if err != nil {
		return nil, err
	}

	return &v, nil
}
