package recaptcha

type verifyReCAPTCHAParams struct {
	Secret string `json:"secret"`
	Token  string `json:"token"`
}

type verifyReCAPTCHAResult struct {
	Success  bool    `json:"success"`
	Score    float64 `json:"score"`
	Hostname string  `json:"hostname"`
}
