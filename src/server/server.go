package server

import (
	"strings"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"gitlab.com/zenkilies/sylens-api/src/internal/config"
)

func NewServer() *echo.Echo {
	deps := initDeps()

	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: strings.Split(config.GetString(config.CORS_ORIGINS, "http://localhost"), ","),
		AllowHeaders: []string{
			echo.HeaderOrigin,
			echo.HeaderContentType,
			echo.HeaderAccept,
		},
		AllowCredentials: true,
	}))

	e.POST("/pastes", deps.pasteHandler.CreatePaste)
	e.GET("/pastes/:slug", deps.pasteHandler.GetPublicPaste)
	e.POST("/pastes/:slug/unlock", deps.pasteHandler.GetProtectedPaste)

	return e
}
