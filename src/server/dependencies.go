package server

import (
	"fmt"
	"log"

	pastehandler "gitlab.com/zenkilies/sylens-api/src/logic/handlers/paste"
	pasteresource "gitlab.com/zenkilies/sylens-api/src/logic/resources/paste"

	"gitlab.com/zenkilies/sylens-api/src/external/mongodb"
	"gitlab.com/zenkilies/sylens-api/src/external/recaptcha"
	"gitlab.com/zenkilies/sylens-api/src/storage"
)

type depsCollector struct {
	// External
	recaptchaClient recaptcha.Client
	mongoClient     mongodb.Client

	// Storage
	pasteStorage storage.PasteStorage

	// Resource
	pasteResource pasteresource.Resource

	// Handler
	pasteHandler pastehandler.Handler
}

func initDeps() depsCollector {
	errorHandler := func(name string, err error) {
		log.Fatal(fmt.Sprintf("failed to init %s with error: %s", name, err.Error()))
	}

	mongoClient, err := mongodb.NewClient()
	if err != nil {
		errorHandler("mongodb.Client", err)
	}

	recaptchaClient, err := recaptcha.NewClient()
	if err != nil {
		errorHandler("recaptcha.Client", err)
	}

	pasteStorage, err := storage.NewPasteStorage(mongoClient)
	if err != nil {
		errorHandler("storage.PasteStorage", err)
	}

	pasteResource, err := pasteresource.NewResource(
		pasteresource.WithPasteStorage(pasteStorage),
	)
	if err != nil {
		errorHandler("pasteresource.Resource", err)
	}

	pasteHandler, err := pastehandler.NewHandler(
		pastehandler.WithReCAPTCHA(recaptchaClient),
		pastehandler.WithPasteResource(pasteResource),
	)
	if err != nil {
		errorHandler("pastehandler.Handler", err)
	}

	deps := depsCollector{
		mongoClient:     mongoClient,
		recaptchaClient: recaptchaClient,
		pasteStorage:    pasteStorage,
		pasteResource:   pasteResource,
		pasteHandler:    pasteHandler,
	}

	return deps
}
