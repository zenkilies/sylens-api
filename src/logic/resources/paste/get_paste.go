package paste

import (
	"context"

	"gitlab.com/zenkilies/sylens-api/src/internal/password"
)

func (i *resourceImpl) GetPaste(ctx context.Context, req GetPasteParams) (*Paste, error) {
	p, err := i.pasteStorage.FindBySlug(ctx, req.Slug)
	if err != nil {
		return nil, err
	}

	if p.Password != "" {
		if req.Password == "" {
			return nil, ERR_UNAUTHORIZED
		}

		if password.CompareHashAndPassword(p.Password, req.Password) != true {
			return nil, ERR_UNAUTHORIZED
		}
	}

	res := &Paste{}
	res.FromStorageDTO(p)

	return res, nil
}
