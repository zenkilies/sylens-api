package paste

import (
	"context"
	"math/rand"
	"time"

	"gitlab.com/zenkilies/sylens-api/src/internal/config"
	"gitlab.com/zenkilies/sylens-api/src/internal/password"
)

func (i *resourceImpl) CreatePaste(ctx context.Context, req CreatePasteParams) (string, error) {
	p := Paste{
		Slug:      i.randomSlug(),
		Content:   req.Content,
		CreatedAt: time.Now().Unix(),
		UpdatedAt: time.Now().Unix(),
	}

	if req.Password != "" {
		p.Password = password.GenerateHashFromPassword(req.Password)
	}

	err := i.pasteStorage.Insert(ctx, p.ToStorageDTO())
	if err != nil {
		return "", err
	}

	return p.Slug, nil
}

func (i *resourceImpl) randomSlug() string {
	const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	b := make([]byte, config.GetInt(config.PASTE_SLUG_LENGTH, 6))
	for i := range b {
		b[i] = charset[r.Intn(len(charset))]
	}

	return string(b)
}
