package paste

import (
	"context"
	"errors"
	"fmt"
	"strings"

	"gitlab.com/zenkilies/sylens-api/src/storage"
)

//go:generate mockery -name=Resource -inpkg -case=snake
type Resource interface {
	CreatePaste(context.Context, CreatePasteParams) (string, error)
	GetPaste(context.Context, GetPasteParams) (*Paste, error)
}

func NewResource(opts ...Option) (Resource, error) {
	i := &resourceImpl{}
	for _, opt := range opts {
		opt(i)
	}

	err := validateDeps(i)
	if err != nil {
		return nil, err
	}

	return i, nil
}

type resourceImpl struct {
	pasteStorage storage.PasteStorage
}

func validateDeps(i *resourceImpl) error {
	var missingDeps []string

	if i.pasteStorage == nil {
		missingDeps = append(missingDeps, "storage.PasteStorage")
	}

	if len(missingDeps) > 0 {
		return errors.New(fmt.Sprintf("missing required dependencies: %s", strings.Join(missingDeps, ", ")))
	}

	return nil
}
