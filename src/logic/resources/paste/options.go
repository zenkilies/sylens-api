package paste

import "gitlab.com/zenkilies/sylens-api/src/storage"

type Option func(impl *resourceImpl)

func WithPasteStorage(pasteStorage storage.PasteStorage) Option {
	return func(impl *resourceImpl) {
		impl.pasteStorage = pasteStorage
	}
}
