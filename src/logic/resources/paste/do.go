package paste

import "gitlab.com/zenkilies/sylens-api/src/storage"

type Paste struct {
	Slug      string
	Content   string
	Password  string
	CreatedAt int64
	UpdatedAt int64
}

func (p *Paste) FromStorageDTO(target *storage.Paste) {
	if target == nil {
		return
	}

	p.Slug = target.Slug
	p.Content = target.Content
	p.Password = target.Password
	p.CreatedAt = target.CreatedAt
	p.UpdatedAt = target.UpdatedAt
}

func (p *Paste) ToStorageDTO() storage.Paste {
	return storage.Paste{
		Slug:      p.Slug,
		Content:   p.Content,
		Password:  p.Password,
		CreatedAt: p.CreatedAt,
		UpdatedAt: p.UpdatedAt,
	}
}

type CreatePasteParams struct {
	Content  string
	Password string
}

type GetPasteParams struct {
	Slug     string
	Password string
}
