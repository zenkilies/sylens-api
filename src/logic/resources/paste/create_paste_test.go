package paste

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

func TestCreatePaste(t *testing.T) {
	t.Parallel()

	var (
		content  = "Lorem ipsum dolor sit amet"
		password = "password"
	)

	type testCase struct {
		desc        string
		doMocks     mockTestFunc
		req         CreatePasteParams
		expectedErr error
	}

	testCases := []testCase{
		{
			desc: "happy path, without password",
			doMocks: func(m mockTestFuncDeps) {
				m.pasteStorage.On("Insert", mock.Anything, mock.Anything).Return(nil)
			},
			req: CreatePasteParams{
				Content: content,
			},
			expectedErr: nil,
		},
		{
			desc: "happy path, with password",
			doMocks: func(m mockTestFuncDeps) {
				m.pasteStorage.On("Insert", mock.Anything, mock.Anything).Return(nil)
			},
			req: CreatePasteParams{
				Content:  content,
				Password: password,
			},
			expectedErr: nil,
		},
		{
			desc: "paste storage returns error",
			doMocks: func(m mockTestFuncDeps) {
				m.pasteStorage.On("Insert", mock.Anything, mock.Anything).Return(errors.New("un-expected error"))
			},
			req: CreatePasteParams{
				Content: content,
			},
			expectedErr: errors.New("un-expected error"),
		},
	}

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.desc, func(t *testing.T) {
			m := mockDeps()
			if testCase.doMocks != nil {
				testCase.doMocks(m)
			}

			r := mockNew(m)
			res, err := r.CreatePaste(context.TODO(), testCase.req)

			assert.Equal(t, testCase.expectedErr, err)

			if testCase.expectedErr == nil {
				assert.NotEmpty(t, res)
			}

			assertExpectationForMocks(t, m)
		})
	}
}
