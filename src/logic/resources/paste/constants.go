package paste

import "errors"

var (
	ERR_UNAUTHORIZED = errors.New("unauthorized")
)
