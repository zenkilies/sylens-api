package paste

import (
	"testing"

	"github.com/stretchr/testify/mock"

	"gitlab.com/zenkilies/sylens-api/src/storage"
)

type mockTestFuncDeps struct {
	pasteStorage *storage.MockPasteStorage
}

type mockTestFunc func(m mockTestFuncDeps)

func mockDeps() mockTestFuncDeps {
	pasteStorageMocks := &storage.MockPasteStorage{}

	return mockTestFuncDeps{
		pasteStorage: pasteStorageMocks,
	}
}

func mockNew(m mockTestFuncDeps) Resource {
	return &resourceImpl{
		pasteStorage: m.pasteStorage,
	}
}

func assertExpectationForMocks(t *testing.T, m mockTestFuncDeps) {
	mock.AssertExpectationsForObjects(t,
		m.pasteStorage,
	)
}
