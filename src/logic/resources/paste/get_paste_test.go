package paste

import (
	"context"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/zenkilies/sylens-api/src/internal/password"
	"gitlab.com/zenkilies/sylens-api/src/storage"
)

func TestGetPaste(t *testing.T) {
	t.Parallel()

	var (
		content      = "Lorem ipsum dolor sit amet"
		passwordHash = password.GenerateHashFromPassword("password")
		slug         = "abc123"
	)

	type testCase struct {
		desc        string
		doMocks     mockTestFunc
		req         GetPasteParams
		expectedRes *Paste
		expectedErr error
	}

	testCases := []testCase{
		{
			desc: "happy path",
			doMocks: func(m mockTestFuncDeps) {
				m.pasteStorage.On("FindBySlug", mock.Anything, slug).Return(&storage.Paste{
					Slug:    slug,
					Content: content,
				}, nil)
			},
			req: GetPasteParams{
				Slug: slug,
			},
			expectedRes: &Paste{
				Slug:    slug,
				Content: content,
			},
			expectedErr: nil,
		},
		{
			desc: "paste storage returns error",
			doMocks: func(m mockTestFuncDeps) {
				m.pasteStorage.On("FindBySlug", mock.Anything, slug).Return(nil, errors.New("un-expected error"))
			},
			req: GetPasteParams{
				Slug: slug,
			},
			expectedRes: nil,
			expectedErr: errors.New("un-expected error"),
		},
		{
			desc: "protected paste, without password",
			doMocks: func(m mockTestFuncDeps) {
				m.pasteStorage.On("FindBySlug", mock.Anything, slug).Return(&storage.Paste{
					Slug:     slug,
					Content:  content,
					Password: passwordHash,
				}, nil)
			},
			req: GetPasteParams{
				Slug: slug,
			},
			expectedRes: nil,
			expectedErr: ERR_UNAUTHORIZED,
		},
		{
			desc: "protected paste, with wrong password",
			doMocks: func(m mockTestFuncDeps) {
				m.pasteStorage.On("FindBySlug", mock.Anything, slug).Return(&storage.Paste{
					Slug:     slug,
					Content:  content,
					Password: passwordHash,
				}, nil)
			},
			req: GetPasteParams{
				Slug:     slug,
				Password: "wrong-password",
			},
			expectedRes: nil,
			expectedErr: ERR_UNAUTHORIZED,
		},
		{
			desc: "protected paste, with correct password",
			doMocks: func(m mockTestFuncDeps) {
				m.pasteStorage.On("FindBySlug", mock.Anything, slug).Return(&storage.Paste{
					Slug:     slug,
					Content:  content,
					Password: passwordHash,
				}, nil)
			},
			req: GetPasteParams{
				Slug:     slug,
				Password: "password",
			},
			expectedRes: &Paste{
				Slug:     slug,
				Content:  content,
				Password: passwordHash,
			},
			expectedErr: nil,
		},
	}

	for _, testCase := range testCases {
		testCase := testCase

		t.Run(testCase.desc, func(t *testing.T) {
			m := mockDeps()
			if testCase.doMocks != nil {
				testCase.doMocks(m)
			}

			r := mockNew(m)
			res, err := r.GetPaste(context.TODO(), testCase.req)

			assert.Equal(t, testCase.expectedRes, res)
			assert.Equal(t, testCase.expectedErr, err)
		})
	}
}
