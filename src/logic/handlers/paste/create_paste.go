package paste

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"

	pasteresource "gitlab.com/zenkilies/sylens-api/src/logic/resources/paste"
)

func (i *handlerImpl) CreatePaste(c echo.Context) error {
	var req CreatePasteRequest
	if err := c.Bind(&req); err != nil {
		return c.NoContent(http.StatusUnprocessableEntity)
	}

	if v, err := i.recaptchaClient.Verify(req.ReCAPTCHA); v != true {
		if err != nil {
			c.Logger().Error(fmt.Sprintf("failed to verify captcha token with error: %s", err.Error()))
		}

		return c.NoContent(http.StatusBadRequest)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*8)
	defer cancel()

	slug, err := i.pasteResource.CreatePaste(ctx, pasteresource.CreatePasteParams{
		Content:  req.Content,
		Password: req.Password,
	})
	if err != nil {
		c.Logger().Error(fmt.Sprintf("failed to create paste with error: %s", err.Error()))
		return c.NoContent(http.StatusInternalServerError)
	}

	return c.JSON(http.StatusOK, CreatePasteResponse{
		Slug: slug,
	})
}
