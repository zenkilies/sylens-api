package paste

import (
	"errors"
	"fmt"
	"strings"

	"github.com/labstack/echo/v4"

	"gitlab.com/zenkilies/sylens-api/src/external/recaptcha"
	pasteresource "gitlab.com/zenkilies/sylens-api/src/logic/resources/paste"
)

//go:generate mockery -name=Handler -inpkg -case=snake
type Handler interface {
	CreatePaste(echo.Context) error
	GetProtectedPaste(echo.Context) error
	GetPublicPaste(echo.Context) error
}

func NewHandler(opts ...Option) (Handler, error) {
	i := &handlerImpl{}
	for _, opt := range opts {
		opt(i)
	}

	err := validateDeps(i)
	if err != nil {
		return nil, err
	}

	return i, nil
}

type handlerImpl struct {
	recaptchaClient recaptcha.Client
	pasteResource   pasteresource.Resource
}

func validateDeps(i *handlerImpl) error {
	var missingDeps []string

	if i.recaptchaClient == nil {
		missingDeps = append(missingDeps, "recaptcha.Client")
	}

	if i.pasteResource == nil {
		missingDeps = append(missingDeps, "pasteresource.Resource")
	}

	if len(missingDeps) > 0 {
		return errors.New(fmt.Sprintf("missing required dependencies: %s", strings.Join(missingDeps, ", ")))
	}

	return nil
}
