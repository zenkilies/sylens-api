package paste

import (
	"gitlab.com/zenkilies/sylens-api/src/external/recaptcha"
	pasteresource "gitlab.com/zenkilies/sylens-api/src/logic/resources/paste"
)

type Option func(i *handlerImpl)

func WithReCAPTCHA(recaptchaClient recaptcha.Client) Option {
	return func(i *handlerImpl) {
		i.recaptchaClient = recaptchaClient
	}
}

func WithPasteResource(pasteResource pasteresource.Resource) Option {
	return func(i *handlerImpl) {
		i.pasteResource = pasteResource
	}
}
