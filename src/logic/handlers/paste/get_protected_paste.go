package paste

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"

	pasteresource "gitlab.com/zenkilies/sylens-api/src/logic/resources/paste"
)

func (i *handlerImpl) GetProtectedPaste(c echo.Context) error {
	slug := c.Param("slug")
	if slug == "" {
		return c.NoContent(http.StatusNotFound)
	}

	var req GetProtectedPasteRequest
	if err := c.Bind(&req); err != nil {
		return c.NoContent(http.StatusUnprocessableEntity)
	}

	if v, err := i.recaptchaClient.Verify(req.ReCAPTCHA); v != true {
		if err != nil {
			c.Logger().Error(fmt.Sprintf("failed to verify captcha token with error: %s", err.Error()))
		}

		return c.NoContent(http.StatusBadRequest)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	p, err := i.pasteResource.GetPaste(ctx, pasteresource.GetPasteParams{
		Slug:     slug,
		Password: req.Password,
	})
	if err != nil {
		if err == pasteresource.ERR_UNAUTHORIZED {
			return c.NoContent(http.StatusUnauthorized)
		}

		return c.NoContent(http.StatusNotFound)
	}

	return c.JSON(http.StatusOK, GetProtectedPasteResponse{
		Content: p.Content,
	})
}
