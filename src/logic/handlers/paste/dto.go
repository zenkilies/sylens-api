package paste

type CreatePasteRequest struct {
	Content   string `json:"content" validate:"required"`
	Password  string `json:"password"`
	ReCAPTCHA string `json:"_grecaptcha"`
}

type CreatePasteResponse struct {
	Slug string `json:"slug"`
}

type GetPublicPasteResponse struct {
	Content string `json:"content"`
}

type GetProtectedPasteRequest struct {
	Password  string `json:"password" validate:"required"`
	ReCAPTCHA string `json:"_grecaptcha"`
}

type GetProtectedPasteResponse struct {
	Content string `json:"content"`
}
