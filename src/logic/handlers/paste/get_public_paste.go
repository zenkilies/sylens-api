package paste

import (
	"context"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"

	pasteresource "gitlab.com/zenkilies/sylens-api/src/logic/resources/paste"
)

func (i *handlerImpl) GetPublicPaste(c echo.Context) error {
	slug := c.Param("slug")
	if slug == "" {
		return c.NoContent(http.StatusNotFound)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()

	p, err := i.pasteResource.GetPaste(ctx, pasteresource.GetPasteParams{
		Slug: slug,
	})
	if err != nil {
		if err == pasteresource.ERR_UNAUTHORIZED {
			return c.NoContent(http.StatusUnauthorized)
		}

		return c.NoContent(http.StatusNotFound)
	}

	return c.JSON(http.StatusOK, GetPublicPasteResponse{
		Content: p.Content,
	})
}
