package main

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"

	"gitlab.com/zenkilies/sylens-api/src/server"
)

func main() {
	if err := godotenv.Load(); err != nil {
		fmt.Println(fmt.Sprintf("WARN: `godotenv.Load` has been failed: %s", err.Error()))
	}

	e := server.NewServer()

	addr := ":1323"
	if port := os.Getenv("PORT"); port != "" {
		addr = fmt.Sprintf(":%s", port)
	}

	e.Logger.Fatal(e.Start(addr))
}
